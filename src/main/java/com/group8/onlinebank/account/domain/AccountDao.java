package com.group8.onlinebank.account.domain;

import com.group8.onlinebank.account.infra.entity.AccountEntity;

import java.util.List;

public interface AccountDao {

    AccountEntity store(Account account);
    List<AccountEntity> getAllUserAccount(String customerId);
    AccountEntity getUserAccountByAccountNumber(String accountNumber);
    void updateUserAccountBalance(String accountNumber, double accountBalance);
}
