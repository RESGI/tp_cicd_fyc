package com.group8.onlinebank.account.domain;

import java.time.LocalDateTime;
import java.util.UUID;

public class AccountBuilder {
    private UUID id;
    private String userId;
    private String accountNumber;
    private AccountType accountType;
    private double accountBalance;
    private AccountStatus accountStatus;

    private LocalDateTime created_at;
    private LocalDateTime updated_at;

    public AccountBuilder setId(UUID id) {
        this.id = id;
        return this;
    }

    public AccountBuilder setUserId(String userId) {
        this.userId = userId;
        return this;
    }

    public AccountBuilder setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
        return this;
    }

    public AccountBuilder setAccountType(AccountType accountType) {
        this.accountType = accountType;
        return this;
    }

    public AccountBuilder setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
        return this;
    }

    public AccountBuilder setAccountStatus(AccountStatus accountStatus) {
        this.accountStatus = accountStatus;
        return this;
    }

    public AccountBuilder setCreated_at(LocalDateTime created_at) {
        this.created_at = created_at;
        return this;
    }

    public AccountBuilder setUpdated_at(LocalDateTime updated_at) {
        this.updated_at = updated_at;
        return this;
    }

    public Account createAccount() {
        return new Account(id, userId, accountNumber, accountType, accountBalance, accountStatus, created_at, updated_at);
    }
}