package com.group8.onlinebank.account.domain.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class AccountNotExistException extends Exception {
    public AccountNotExistException(String message) {
        super(message);
    }
}
