package com.group8.onlinebank.account.domain;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Accessors(chain = true)
public class Account {

    private UUID id;
    private String userId;
    private String accountNumber;
    private AccountType accountType;
    private double accountBalance;
    private AccountStatus accountStatus;
    private LocalDateTime created_at;
    private LocalDateTime updated_at;

    public Account(UUID id, String userId, String accountNumber, AccountType accountType, double accountBalance,
                   AccountStatus accountStatus, LocalDateTime created_at, LocalDateTime updated_at) {
        this.id = id;
        this.userId = userId;
        this.accountNumber = accountNumber;
        this.accountType = accountType;
        this.accountBalance = accountBalance;
        this.accountStatus = accountStatus;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }
}
