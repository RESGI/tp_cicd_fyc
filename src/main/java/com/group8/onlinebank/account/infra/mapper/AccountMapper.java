package com.group8.onlinebank.account.infra.mapper;

import com.group8.onlinebank.account.domain.Account;
import com.group8.onlinebank.account.domain.AccountBuilder;
import com.group8.onlinebank.account.infra.entity.AccountEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class AccountMapper {


    public Account map(AccountEntity accountEntity) {
        return new AccountBuilder()
                .setId(accountEntity.getId())
                .setAccountNumber(accountEntity.getAccountNumber())
                .setAccountType(accountEntity.getAccountType())
                .setAccountBalance(accountEntity.getAccountBalance())
                .setAccountStatus(accountEntity.getAccountStatus())
                .setUserId(accountEntity.getUserId())
                .setCreated_at(accountEntity.getCreated_at())
                .setUpdated_at(accountEntity.getUpdated_at())
                .createAccount();
    }

    public AccountEntity map(Account account) {
        return new AccountEntity()
                .setUserId(account.getUserId())
                .setAccountType(account.getAccountType())
                .setAccountBalance(account.getAccountBalance())
                .setAccountStatus(account.getAccountStatus());
    }

}
