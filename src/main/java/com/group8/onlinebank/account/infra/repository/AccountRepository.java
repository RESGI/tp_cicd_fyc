package com.group8.onlinebank.account.infra.repository;

import com.group8.onlinebank.account.infra.entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface AccountRepository extends JpaRepository<AccountEntity, UUID> {
    List<AccountEntity> findByUserId(String customerId);
    AccountEntity findByAccountNumber(String accountNumber);
}
