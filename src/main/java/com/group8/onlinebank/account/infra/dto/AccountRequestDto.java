package com.group8.onlinebank.account.infra.dto;

import com.group8.onlinebank.account.domain.AccountStatus;
import com.group8.onlinebank.account.domain.AccountType;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotBlank;
import java.util.UUID;


@Data
@Service
@RequiredArgsConstructor
public class AccountRequestDto {
    @NotBlank
    private String userId;
    @NotBlank
    private AccountType accountType;

    public AccountRequestDto(@NotBlank String userId, @NotBlank AccountType accountType) {
        this.userId = userId;
        this.accountType = accountType;
    }
}
