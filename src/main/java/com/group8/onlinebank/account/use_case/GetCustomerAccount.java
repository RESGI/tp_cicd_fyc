package com.group8.onlinebank.account.use_case;

import com.group8.onlinebank.account.domain.AccountDao;
import com.group8.onlinebank.account.infra.entity.AccountEntity;
import com.group8.onlinebank.auth.domain.UserDAO;
import com.group8.onlinebank.auth.domain.exceptions.UserNotExistException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class GetCustomerAccount {

    private final AccountDao accountDao;
    private final UserDAO userDAO;

    public List<AccountEntity> execute(String customerId) throws UserNotExistException {
        UUID userId = UUID.fromString(customerId);
        if (!userDAO.existByUserId(userId)) {
            throw new UserNotExistException("This user not exist");
        }
        return accountDao.getAllUserAccount(customerId);
    }
}
