package com.group8.onlinebank.account.use_case;

import com.group8.onlinebank.account.domain.AccountDao;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UpdateBalance {

    private final AccountDao accountDao;

    public void execute(String accountNumber, double accountBalance) {
        accountDao.updateUserAccountBalance(accountNumber, accountBalance);
    }

}