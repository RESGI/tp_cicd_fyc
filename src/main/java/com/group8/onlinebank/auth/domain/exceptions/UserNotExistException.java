package com.group8.onlinebank.auth.domain.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotExistException extends Exception {
    public UserNotExistException(String message) {
        super(message);
    }
}
