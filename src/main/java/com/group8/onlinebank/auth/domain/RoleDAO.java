package com.group8.onlinebank.auth.domain;

import com.group8.onlinebank.auth.domain.Role;
import com.group8.onlinebank.auth.domain.RoleEnum;

public interface RoleDAO {
    Role findByRoleName(RoleEnum roleUser);
}
