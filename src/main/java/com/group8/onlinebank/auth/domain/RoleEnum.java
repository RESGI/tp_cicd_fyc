package com.group8.onlinebank.auth.domain;

public enum RoleEnum {
    ROLE_CUSTOMER,
    ROLE_MANAGER
}
