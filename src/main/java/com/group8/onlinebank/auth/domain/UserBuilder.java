package com.group8.onlinebank.auth.domain;

public class UserBuilder {
    private String lastname;
    private String firstname;
    private String phone;
    private String email;
    private String password;
    private String role;

    public UserBuilder setLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public UserBuilder setFirstname(String firstname) {
        this.firstname = firstname;
        return this;
    }

    public UserBuilder setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public UserBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public UserBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public UserBuilder setRole(String role) {
        this.role = role;
        return this;
    }

    public User createUser() {
        return new User(lastname, firstname, phone, email, password, role);
    }
}