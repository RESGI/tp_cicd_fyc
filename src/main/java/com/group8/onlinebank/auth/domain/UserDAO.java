package com.group8.onlinebank.auth.domain;

import java.util.UUID;

public interface UserDAO {
    User createUser(String lastname, String firstname, String phone, String email, String password, String role);
    boolean existsByEmail(String email);
    boolean existByUserId(UUID userId);
}
