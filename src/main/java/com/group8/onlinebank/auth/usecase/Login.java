package com.group8.onlinebank.auth.usecase;

import com.group8.onlinebank.auth.infra.dto.LoginRequestDto;
import com.group8.onlinebank.auth.infra.dto.LoginResponseDto;
import com.group8.onlinebank.security.jwt.JwtUtils;
import com.group8.onlinebank.security.services.UserDetailsImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class Login {

    private final AuthenticationManager authenticationManager;

    private final JwtUtils jwtUtils;

    public LoginResponseDto execute(LoginRequestDto loginRequestDto) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequestDto.getEmail(), loginRequestDto.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        Optional<? extends GrantedAuthority> role = userDetails.getAuthorities().stream().findFirst();

        return new LoginResponseDto(jwt,
                userDetails.getId(),
                userDetails.getFirstname(),
                userDetails.getLastname(),
                userDetails.getPhone(),
                userDetails.getUsername(),
                role);
    }
}
