package com.group8.onlinebank.auth.usecase;

import com.group8.onlinebank.auth.domain.UserDAO;
import com.group8.onlinebank.auth.domain.exceptions.UserExistException;
import com.group8.onlinebank.auth.infra.dto.RegisterRequestDto;
import com.group8.onlinebank.auth.domain.User;
import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class Register {

    private final UserDAO userDAO;

    private final PasswordEncoder encoder;

    public User execute(RegisterRequestDto registerRequestDto) throws UserExistException, NotFoundException {

        checkIfEmailAlreadyExist(registerRequestDto.getEmail());
        checkIfRoleInListContainManageList(registerRequestDto.getRole(), Arrays.asList("customer", "manager"));

        return userDAO.createUser(
                registerRequestDto.getLastname(),
                registerRequestDto.getFirstname(),
                registerRequestDto.getPhone(),
                registerRequestDto.getEmail(),
                encoder.encode(registerRequestDto.getPassword()),
                registerRequestDto.getRole()
                );

    }

    private void checkIfEmailAlreadyExist(String email) throws UserExistException {
        if (userDAO.existsByEmail(email)) {
            String exceptionMessage = String.format("User with email '%s' already exists", email);
            throw new UserExistException(exceptionMessage);
        }
    }

    private void checkIfRoleInListContainManageList(String userRole, List<String> availableRoles) throws NotFoundException {
        if (userRole == null) return;
        Optional<String> foundRole = availableRoles.stream()
                .filter(role -> role.equals(userRole))
                .findFirst();
        if (!foundRole.isPresent()) {
            String message = String.format("Role name '%s' not found", userRole);
            throw new NotFoundException(message);
        }

    }


}
