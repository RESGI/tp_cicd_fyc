package com.group8.onlinebank.auth.infra.dao;

import com.group8.onlinebank.auth.domain.Role;
import com.group8.onlinebank.auth.domain.RoleDAO;
import com.group8.onlinebank.auth.domain.RoleEnum;
import com.group8.onlinebank.auth.infra.repository.RoleRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@AllArgsConstructor
@Service
public class RoleDAOImpl implements RoleDAO {

    private final RoleRepository roleRepository;

    @Override
    public Role findByRoleName(RoleEnum roleUser) {
        return roleRepository.findByName(roleUser).orElse(null);
    }
}
