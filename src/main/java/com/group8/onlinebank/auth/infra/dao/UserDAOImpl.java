package com.group8.onlinebank.auth.infra.dao;

import com.group8.onlinebank.auth.domain.User;
import com.group8.onlinebank.auth.domain.UserBuilder;
import com.group8.onlinebank.auth.domain.UserDAO;
import com.group8.onlinebank.auth.infra.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@AllArgsConstructor
public class UserDAOImpl implements UserDAO {

    private final UserRepository userRepository;

    @Override
    public User createUser(String lastname, String firstname, String phone, String email, String password, String role) {
        User user = new UserBuilder()
                .setEmail(email)
                .setFirstname(firstname)
                .setLastname(lastname)
                .setPhone(phone)
                .setPassword(password)
                .setRole(role)
                .createUser();

        return userRepository.save(user);
    }

    @Override
    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public boolean existByUserId(UUID userId) {
        return userRepository.existsById(userId);
    }
}
