package com.group8.onlinebank.auth.infra.controller;

import com.group8.onlinebank.account.domain.Account;
import com.group8.onlinebank.account.domain.AccountStatus;
import com.group8.onlinebank.account.domain.AccountType;
import com.group8.onlinebank.account.infra.dto.AccountRequestDto;
import com.group8.onlinebank.account.infra.entity.AccountEntity;
import com.group8.onlinebank.account.infra.mapper.AccountMapper;
import com.group8.onlinebank.account.use_case.CreateAccount;
import com.group8.onlinebank.auth.domain.exceptions.UserExistException;
import com.group8.onlinebank.auth.domain.exceptions.UserNotExistException;
import com.group8.onlinebank.auth.infra.dto.LoginRequestDto;
import com.group8.onlinebank.auth.infra.dto.RegisterRequestDto;
import com.group8.onlinebank.auth.infra.dto.LoginResponseDto;
import com.group8.onlinebank.auth.infra.dto.RegisterResponseDto;
import com.group8.onlinebank.auth.domain.User;
import com.group8.onlinebank.auth.usecase.Login;
import com.group8.onlinebank.auth.usecase.Register;
import javassist.NotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@AllArgsConstructor
@RequestMapping("/public/v0/auth")
public class AuthController {

    private final Login login;

    private final Register register;
    private final CreateAccount createAccount;
    private final AccountMapper accountMapper;


    @PostMapping("/login")
    public ResponseEntity<LoginResponseDto> authenticateUser(@Valid @RequestBody LoginRequestDto loginRequestDto) {
        return ResponseEntity.ok(login.execute(loginRequestDto));

    }

    @PostMapping("/register")
    public ResponseEntity<?> registerUser(@Valid @RequestBody RegisterRequestDto signUpRequest) throws NotFoundException, UserExistException, UserNotExistException {

        signUpRequest.setRole("customer");
        User user = register.execute(signUpRequest);


        AccountRequestDto accountRequestDto = new AccountRequestDto(user.getId().toString(), AccountType.ACCOUNT_TYPE_COURANT);
        AccountEntity accountEntity = createAccount.execute(accountRequestDto);

        return ResponseEntity.ok(
                new RegisterResponseDto(
                        user.getId(),
                        "User registered successfully!",
                        accountMapper.map(accountEntity)));
    }
}
