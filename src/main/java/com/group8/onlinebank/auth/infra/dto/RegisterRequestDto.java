package com.group8.onlinebank.auth.infra.dto;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
public class RegisterRequestDto {

    @NotBlank
    @Size(max = 80)
    private String lastname;

    @NotBlank
    @Size(max = 20)
    private String firstname;

    @NotBlank
    @Pattern(regexp = "(^$|[0-9]{10})")
    private String phone;

    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    @NotBlank
    @Size(min = 8, max = 40)
    private String password;

    private String role;
}
