package com.group8.onlinebank.transaction.domain;

public enum TransactionStatus {
    TRANSACTION_STATUS_APPROVED,
    TRANSACTION_STATUS_PENDING,
    TRANSACTION_STATUS_REFUSED
}
