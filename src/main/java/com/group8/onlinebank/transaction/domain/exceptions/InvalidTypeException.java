package com.group8.onlinebank.transaction.domain.exceptions;

public class InvalidTypeException extends Exception {
    public InvalidTypeException(String message) {
        super(message);
    }
}
