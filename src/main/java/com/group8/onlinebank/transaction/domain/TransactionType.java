package com.group8.onlinebank.transaction.domain;

public enum TransactionType {
    TRANSACTION_TYPE_PAYMENT,
    TRANSACTION_TYPE_WITHDRAWAL,
    TRANSACTION_TYPE_TRANSFER
}
