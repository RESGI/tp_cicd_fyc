package com.group8.onlinebank.transaction.domain;

import lombok.Data;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Accessors(chain = true)
public class Transaction {

    private UUID id;
    private UUID userId;
    private String accountNumber;
    private String recipientAccountNumber;
    private double amount;
    private TransactionType type;
    private TransactionStatus status;
    private LocalDateTime created_at;
    private LocalDateTime updated_at;





}
