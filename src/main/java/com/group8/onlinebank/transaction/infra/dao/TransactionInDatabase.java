package com.group8.onlinebank.transaction.infra.dao;

import com.group8.onlinebank.transaction.domain.Transaction;
import com.group8.onlinebank.transaction.domain.TransactionDao;
import com.group8.onlinebank.transaction.infra.entity.TransactionEntity;
import com.group8.onlinebank.transaction.infra.mapper.TransactionMapper;
import com.group8.onlinebank.transaction.infra.repository.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

@Repository
@RequiredArgsConstructor
public class TransactionInDatabase implements TransactionDao {

    private final TransactionRepository transactionRepository;
    private final TransactionMapper transactionMapper;

    @Override
    public TransactionEntity store(Transaction transaction) {
        return transactionRepository.save(transactionMapper.map(transaction));
    }

    @Override
    public Optional<TransactionEntity> getTransactionById(UUID transactionId) {
        return transactionRepository.findById(transactionId);
    }

    @Override
    public List<Transaction> getAllByAccountNumber(String accountNumber) {
        return transactionRepository.findByAccountNumber(accountNumber)
                .stream()
                .map(transactionMapper::map)
                .collect(toList());
    }

    @Override
    public List<Transaction> getAllByUserId(UUID userId) {
        return transactionRepository.findByUserId(userId)
                .stream()
                .map(transactionMapper::map)
                .collect(toList());
    }

    @Override
    public TransactionEntity updateTransaction(Transaction transaction) {
        return transactionRepository.save(transactionMapper.map(transaction));
    }


}
