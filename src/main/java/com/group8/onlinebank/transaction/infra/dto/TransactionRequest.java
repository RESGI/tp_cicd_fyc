package com.group8.onlinebank.transaction.infra.dto;

import com.group8.onlinebank.transaction.domain.TransactionStatus;
import com.group8.onlinebank.transaction.domain.TransactionType;
import lombok.Data;
import org.hibernate.validator.constraints.Range;
import org.springframework.stereotype.Service;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.UUID;

@Data
@Service
public class TransactionRequest {

    @NotBlank
    private String userId;

    @NotBlank
    @Size(min = 30)
    private String accountNumber;

    @Size(min = 30)
    private String recipientAccountNumber;

    @DecimalMin("0.0")
    private double amount;

    private TransactionType type;

    private TransactionStatus status;

    public String getAccount_number(){
        return this.accountNumber;
    }
}
