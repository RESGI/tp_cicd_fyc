package com.group8.onlinebank.transaction.infra.mapper;

import com.group8.onlinebank.transaction.domain.Transaction;
import com.group8.onlinebank.transaction.infra.entity.TransactionEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TransactionMapper {

    public Transaction map(TransactionEntity transactionEntity) {
        return new Transaction()
                .setId(transactionEntity.getId())
                .setUserId(transactionEntity.getUserId())
                .setAccountNumber(transactionEntity.getAccountNumber())
                .setRecipientAccountNumber(transactionEntity.getRecipientAccountNumber())
                .setAmount(transactionEntity.getAmount())
                .setType(transactionEntity.getType())
                .setStatus(transactionEntity.getStatus())
                .setCreated_at(transactionEntity.getCreated_at())
                .setUpdated_at(transactionEntity.getUpdated_at());
    }

    public TransactionEntity map(Transaction transaction) {
        return new TransactionEntity()
                .setId(transaction.getId())
                .setUserId(transaction.getUserId())
                .setAccountNumber(transaction.getAccountNumber())
                .setRecipientAccountNumber(transaction.getRecipientAccountNumber())
                .setAmount(transaction.getAmount())
                .setType(transaction.getType())
                .setStatus(transaction.getStatus());
    }
}
