package com.group8.onlinebank.Taux_de_change.use_case;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;


@Service
@RequiredArgsConstructor
public class GetCurrencyRates {
    //private final RateDao rateDao;

    public HashMap<String, ?> execute(){

        final String uri = "http://data.fixer.io/api/latest?access_key=8e9242467a4b907acfa4c5d21b48f8ea&format=1";

        RestTemplate restTemplate = new RestTemplate();
        HashMap<String, ?> result = restTemplate.getForObject(uri, HashMap.class);


        return result;
    }

}

