package com.group8.onlinebank.client.infrastructure.controller;
import com.group8.onlinebank.account.domain.AccountType;
import com.group8.onlinebank.account.domain.exceptions.AccountNotExistException;
import com.group8.onlinebank.account.infra.dto.AccountRequestDto;
import com.group8.onlinebank.account.use_case.CreateAccount;
import com.group8.onlinebank.account.use_case.GetCustomerAccount;
import com.group8.onlinebank.auth.domain.exceptions.UserNotExistException;
import com.group8.onlinebank.client.domain.exceptions.InsufficientFundsException;
import com.group8.onlinebank.client.uses_cases.GetBalance;
import com.group8.onlinebank.client.uses_cases.Payment;
import com.group8.onlinebank.client.uses_cases.Transfer;
import com.group8.onlinebank.client.uses_cases.Withdrawal;
import com.group8.onlinebank.transaction.domain.TransactionType;
import com.group8.onlinebank.transaction.domain.exceptions.InvalidTypeException;
import com.group8.onlinebank.transaction.infra.dto.TransactionRequest;
import com.group8.onlinebank.transaction.use_case.GetCustomerTransactions;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@AllArgsConstructor
@RequestMapping("/public/v0/customer")
public class CustomerController {

    Payment payment;
    Withdrawal withdrawal;
    GetBalance getBalance;
    Transfer transfer;
    CreateAccount createAccount;
    GetCustomerAccount getCustomerAccount;
    GetCustomerTransactions getCustomerTransactions;
    AccountRequestDto accountRequestDto;

    @ApiOperation(value = "Créer un compte epargne pour le user dont l'id est customerID")
    @PostMapping("/account/new/{customerId}")
    public ResponseEntity<?> account(@PathVariable String customerId) throws InvalidTypeException, AccountNotExistException, UserNotExistException {

        accountRequestDto.setUserId(customerId);
        accountRequestDto.setAccountType(AccountType.ACCOUNT_TYPE_EPARGNE);

        return ResponseEntity.ok(createAccount.execute(accountRequestDto));
    }

    @ApiOperation(value = "effectuer un virement ")
    @PostMapping("/payment")
    public ResponseEntity<?> payment(@Valid @RequestBody TransactionRequest transactionRequest) throws InvalidTypeException, AccountNotExistException, UserNotExistException {

        transactionRequest.setType(TransactionType.TRANSACTION_TYPE_PAYMENT);

        return ResponseEntity.ok(payment.execute(transactionRequest));
    }

    @ApiOperation(value = "effectuer un retrait")
    @PostMapping("/withdrawal")
    public ResponseEntity<?> withdrawal(@Valid @RequestBody TransactionRequest transactionRequest) throws InvalidTypeException, InsufficientFundsException, AccountNotExistException, UserNotExistException {

        transactionRequest.setType(TransactionType.TRANSACTION_TYPE_WITHDRAWAL);

        return ResponseEntity.ok(withdrawal.execute(transactionRequest));
    }

    @ApiOperation(value  = "Consulter le Solde du compte dont le numéro est account_number")
    @GetMapping("/balance/{account_number}")
    public ResponseEntity<?> balance(@PathVariable("account_number") String accountNumber) throws InvalidTypeException, InsufficientFundsException, AccountNotExistException, UserNotExistException {
        return ResponseEntity.ok(getBalance.execute(accountNumber));
    }

    @ApiOperation(value  = "Consulter le Solde du compte dont le numéro est account_number à une date donnée")
    @GetMapping("/balance/{account_number}/{date}")
    public ResponseEntity<?> balanceUntilDate(@PathVariable("account_number") String accountNumber, @PathVariable String date) throws InvalidTypeException, InsufficientFundsException, AccountNotExistException, UserNotExistException {
        return ResponseEntity.ok(getBalance.execute(accountNumber, date));
    }

    @ApiOperation(value = "effectuer un transfert d'un compte vers un autre")
    @PostMapping("/transfer")
    public ResponseEntity<?> transfer(@Valid @RequestBody TransactionRequest transactionRequest) throws InvalidTypeException, InsufficientFundsException, AccountNotExistException, UserNotExistException {

        transactionRequest.setType(TransactionType.TRANSACTION_TYPE_TRANSFER);

        return ResponseEntity.ok(transfer.execute(transactionRequest));
    }

    @ApiOperation(value = "afficher tous les comptes d'un utilisateur dont L'id est customerId")
    @GetMapping("/accounts/{customerId}")
    public ResponseEntity<?> accounts(@PathVariable String customerId) throws UserNotExistException {
        return ResponseEntity.ok(getCustomerAccount.execute(customerId));
    }

    @ApiOperation(value = "afficher les transcations du client dont l'id est customerId")
    @GetMapping("/transactions/{customerId}")
    public ResponseEntity<?> transactions(@PathVariable String customerId) throws UserNotExistException {
        return ResponseEntity.ok(getCustomerTransactions.execute(customerId));
    }

}
