package com.group8.onlinebank.client.uses_cases;

import com.group8.onlinebank.account.domain.exceptions.AccountNotExistException;
import com.group8.onlinebank.account.use_case.VerifyAccount;
import com.group8.onlinebank.auth.domain.UserDAO;
import com.group8.onlinebank.auth.domain.exceptions.UserNotExistException;
import com.group8.onlinebank.client.domain.Client;
import com.group8.onlinebank.transaction.domain.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class GetBalance {

    private final TransactionService transactionService;
    private final VerifyAccount verifyAccount;

    public double execute(String accountNumber) throws AccountNotExistException, UserNotExistException {

        boolean accountExist = verifyAccount.execute(accountNumber);
        if (!accountExist) {
            throw new AccountNotExistException("This account was not found.");
        }

        double balance = transactionService.getBalance(accountNumber);

        return balance;
    }

    public double execute(String accountNumber, String date) throws AccountNotExistException, UserNotExistException {

        boolean accountExist = verifyAccount.execute(accountNumber);
        if (!accountExist) {
            throw new AccountNotExistException("This account was not found.");
        }

        double balance = transactionService.getBalance(accountNumber, date);

        return balance;
    }
}
