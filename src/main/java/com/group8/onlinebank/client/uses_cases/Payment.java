package com.group8.onlinebank.client.uses_cases;

import com.group8.onlinebank.account.domain.exceptions.AccountNotExistException;
import com.group8.onlinebank.account.use_case.UpdateBalance;
import com.group8.onlinebank.account.use_case.VerifyAccount;
import com.group8.onlinebank.auth.domain.UserDAO;
import com.group8.onlinebank.auth.domain.exceptions.UserNotExistException;
import com.group8.onlinebank.transaction.domain.TransactionService;
import com.group8.onlinebank.transaction.domain.TransactionStatus;
import com.group8.onlinebank.transaction.domain.TransactionThreshold;
import com.group8.onlinebank.transaction.infra.dto.TransactionRequest;
import com.group8.onlinebank.transaction.infra.dto.TransactionResponse;
import com.group8.onlinebank.transaction.infra.entity.TransactionEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class Payment {

    private final TransactionService transactionService;
    private final VerifyAccount verifyAccount;
    private final UpdateBalance updateBalance;
    private final UserDAO userDAO;

    public TransactionResponse execute(TransactionRequest transactionRequest) throws AccountNotExistException, UserNotExistException {
        UUID userId = UUID.fromString(transactionRequest.getUserId());
        boolean userExist = userDAO.existByUserId(userId);
        if(!userExist) {
            throw new UserNotExistException("This user not found");
        }

        boolean accountExist = verifyAccount.execute(transactionRequest.getAccountNumber());
        if (!accountExist) {
            throw new AccountNotExistException("This account is not found.");
        }

        if(transactionRequest.getAmount() >= TransactionThreshold.PAYMENT_THRESHOLD){
            transactionRequest.setStatus(TransactionStatus.TRANSACTION_STATUS_PENDING);
        } else {
            transactionRequest.setStatus(TransactionStatus.TRANSACTION_STATUS_APPROVED);
        }

        TransactionEntity transactionEntity = transactionService.addTransaction(transactionRequest);
        updateBalance.execute(transactionRequest.getAccountNumber(),
                transactionService.getBalance(transactionRequest.getAccountNumber()));

        return new TransactionResponse(transactionEntity);

    }
}
