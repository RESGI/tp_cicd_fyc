package com.group8.onlinebank.client.domain;

import com.group8.onlinebank.transaction.domain.TransactionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class Client {

    public boolean verifyAccountNumber(String accountNumber) {
        return true;
    }

    public boolean verifyBalance(TransactionService transactionService, String account_number, double withdrawalAmount) {

        //Check if user balance is greater than withdrawal amount
        double balance = transactionService.getBalance(account_number);

        log.warn("balance: " + balance);
        log.warn("withdrawalAmount: " + withdrawalAmount);
        if(balance < withdrawalAmount)
            return false;

        return true;
    }

    public double getBalance(TransactionService transactionService, String accountNumber) {
        return transactionService.getBalance(accountNumber);
    }

}
