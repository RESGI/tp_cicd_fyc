package com.group8.onlinebank.account_manager.builder;

import com.group8.onlinebank.account_manager.domain.AccountManager;

public class AccountManagerBuilder {

    private String name;
    private String email;
    private String password;
    private String phoneNumber;

    public AccountManager build() {
        return new AccountManager(name, email, password, phoneNumber);
    }

    public AccountManagerBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public AccountManagerBuilder withEmail(String email) {
        this.email = email;
        return this;
    }

    public AccountManagerBuilder withPassword(String password) {
        this.password = password;
        return this;
    }

    public AccountManagerBuilder withPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }
}
