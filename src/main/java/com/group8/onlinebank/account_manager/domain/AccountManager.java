package com.group8.onlinebank.account_manager.domain;

import org.springframework.context.annotation.Primary;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

public class AccountManager {

    @Id
    private final UUID id = UUID.randomUUID();
    private String name;
    private String email;
    private String password;
    private String phoneNumber;

    public AccountManager(String name, String email, String password, String phoneNumber) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.phoneNumber = phoneNumber;
    }

    public AccountManager() {

    }
}
