package com.group8.onlinebank.account_manager.usecase;

import com.group8.onlinebank.account.domain.exceptions.AccountNotExistException;
import com.group8.onlinebank.account.use_case.VerifyAccount;
import com.group8.onlinebank.transaction.domain.Transaction;
import com.group8.onlinebank.transaction.domain.TransactionDao;
import com.group8.onlinebank.transaction.domain.TransactionService;
import com.group8.onlinebank.transaction.infra.dto.TransactionRequest;
import com.group8.onlinebank.transaction.infra.entity.TransactionEntity;
import com.group8.onlinebank.transaction.infra.mapper.TransactionMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class GetPendingTransactions {
    private final TransactionDao transactionDao;
    private final VerifyAccount verifyAccount;
    private final TransactionMapper transactionMapper;
    private final TransactionService transactionService;



    public List<TransactionEntity> execute(String accountNumber) throws AccountNotExistException {
        boolean accountExist = verifyAccount.execute(accountNumber);
        if (!accountExist) {
            throw new AccountNotExistException("This account is not found.");
        }

        List<Transaction> transactions  = transactionDao.getAllByAccountNumber(accountNumber);
        List<TransactionEntity> pendingTransactions = transactionService.filterPendingTransactions(transactions).stream().map(transactionMapper::map).collect(Collectors.toList());
        return pendingTransactions;

    }


}
