## Create .env with follow content :

MYSQL_ROOT_PASSWORD=xxx

MYSQL_USER=xxx

MYSQL_PASSWORD=xxxx

MYSQL_HOST=db

MYSQL_DATABASE=online_bank_db


## command to run application 
```
docker-compose  --env-file .env up -d
```

## (Optional) - Recreate app image
```
docker-compose --env-file .env up -d --build
```


### access the swagger documentation on thr URL:
http://localhost:8080/swagger-ui.html


How to test the application :

## For authentication endpoints (for customer or customer manager): 
 * /public/v0/auth/register: registration (le role n'est pas obligatoire à spécifier)
```json
{
"email": "testuser@mail.com",
"firstname": "test",
"lastname": "user",
"password": "Test1234JJS",
"phone": "0123425281", 
"role": "customer" ou ""
}
```

* /public/v0/auth/login: authentication
```json
{
    "email": "testuser@mail.com",
    "password": "Test1234JJS"

}
```

recupérer le JWT Token inclu dans la réponse de la requête login (si response code = 200) , cliquer sur le boutton Authorize en haut de la page 
coller le JWT précédé par le mot Bearer 


